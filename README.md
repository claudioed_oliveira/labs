# Labs
Projeto para a manutenção do cadastro de veículos disponíveis para serem leiloados


## Build imagem docker

Antes de realizar a criação da imagem docker, temos que criar nosso artefator .jar, 
podemos usar o seguinte comando:

```
mvn clean install
```


No diretório raiz do projeto, use o seguinte comando:

```
docker build -t claudioed/labs .
```

## Requisitos para build do projeto
 * java 11
 * mvn > 3.6


## Instruções para rodar a imagem docker

Podemos utilizar o seguinte comando para rodar a imagem:

```
docker run -d -p 9099:9099 -e LEGACY_HOST=https://dev.apiluiza.com.br/legado/veiculo claudioed/labs
```

### Configuração de timeout

Algumas configurações da aplicaçāo podem ser feitas utilizando environment variables
seguindo o princípio III do [12 Factor Apps](https://12factor.net/)

As seguintes varíaveis de ambiente podem ser configuradas para setar o timeout do client HTTP
pode ser utilizado para otimizações.

* CLIENT_CONN_TIMEOUT = valor em milisegundos
* CLIENT_READ_TIMEOUT = valor em milisegundos


## Documentação da API

A documentação da API pode ser encontrada em, depois do container previamente criado:

* [Swagger UI](http://localhost:9099/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config)

O arquivo da especificação foi feito utilizando [Open API Spec](http://openapis.org) na versão 3 e pode ser encontrado

* [OAS](/oas/oas.yaml)

## Infra

As URLs relacionadas a infraestrutura podem ser encontrada em, depois do container previamente criado:

* [URL de Healthcheck](http://localhost:9099/actuator/health)
* [Métricas Prometheus](http://localhost:9099/actuator/prometheus)
  * as métricas de serviço relacionadas ao HTTP podem se encontradas com o seguinte prefixo **http_server_requests** . A URI foi utilizada
  como labels do prometheus para facilitar agregações
    
As métricas utilizam o conceito [RED](https://www.weave.works/blog/the-red-method-key-metrics-for-microservices-architecture/) utilizado para monitorar serviços. 222

## Testes

A collection do postman para testes pode ser encontrada [aqui](/collection/labs.postman_collection.json)