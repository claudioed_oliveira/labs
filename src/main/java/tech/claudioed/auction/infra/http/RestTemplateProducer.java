package tech.claudioed.auction.infra.http;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author claudioed on 07/10/20.
 * Project auction
 */
@Configuration
public class RestTemplateProducer {

    private final int connectionTimeout;

    private final int readTimeout;

    public RestTemplateProducer(@Value("${rest.client.connection.timeout}") int connectionTimeout, @Value("${rest.client.read.timeout}") int readTimeout) {
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }

    @Bean
    public RestTemplate restTemplate(HttpComponentsClientHttpRequestFactory simpleClientHttpRequestFactory) {
        return new RestTemplate(simpleClientHttpRequestFactory);
    }

    @Bean
    public HttpComponentsClientHttpRequestFactory simpleClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(this.connectionTimeout);
        clientHttpRequestFactory.setReadTimeout(this.readTimeout);
        return clientHttpRequestFactory;
    }

}
