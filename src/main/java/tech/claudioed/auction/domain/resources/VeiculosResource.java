package tech.claudioed.auction.domain.resources;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import tech.claudioed.auction.domain.VeiculoData;
import tech.claudioed.auction.domain.data.request.RequestAlteracaoVeiculo;
import tech.claudioed.auction.domain.data.request.RequestNovoVeiculo;
import tech.claudioed.auction.domain.service.VeiculoService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author claudioed on 07/10/20.
 * Project auction
 */
@RestController
@RequestMapping("/api/veiculos")
@Tag(name = "API Veículos", description = "API para manutenção do cadastro de veículos disponíveis para serem leiloados")
public class VeiculosResource {

    private final VeiculoService veiculoService;

    public VeiculosResource(VeiculoService veiculoService) {
        this.veiculoService = veiculoService;
    }

    @GetMapping
    @Operation(tags = "veiculos", operationId = "getVeiculo", description = "Operação para buscar um conjunto de veículos de acordo com os filtros desejados", parameters = {
            @Parameter(name = "lote", description = "Agrupador de um conjunto de veículos, a busca é realizada pelo valor exato"),
            @Parameter(name = "marca", description = "Marca do veículo, a busca é realizada pelo valor exato"),
            @Parameter(name = "modelo", description = "Modelo do veículo, a busca é realizada pelas letras iniciais "),
            @Parameter(name = "anoFabricacao", description = "Ano de fabricação do veículo, essa busca deve ser combinada com o ano modelo  "),
            @Parameter(name = "anoModelo", description = "Ano do modelo do veículo, essa busca deve ser combinada com o ano fabricação "),
            @Parameter(name = "anoFabricacaoDesde", description = "Ano de fabricação do veículo, essa busca deve ser combinada com o anoFabricacaoAte, utilizando o argumento entre anoFabricacaoDesde e anoFabricacaoAte "),
            @Parameter(name = "anoFabricacaoAte", description = "Ano de fabricação do veículo, essa busca deve ser combinada com o ano modelo anoFabricacaoDesde, utilizando o argumento entre anoFabricacaoDesde e anoFabricacaoAte  "),
            @Parameter(name = "sort", description = "ordenação pela data do último lance (ascendente[asc] e descendente[desc])"),
    })
    public ResponseEntity<List<VeiculoData>> veiculos(@RequestParam(value = "lote", required = false) Optional<String> lote, @RequestParam(value = "marca", required = false) Optional<String> marca,
                                                        @RequestParam(value = "modelo", required = false) Optional<String> modelo, @RequestParam(value = "anoFabricacao", required = false) Optional<String> anoFabricacao,
                                                        @RequestParam(value = "anoModelo", required = false) Optional<String> anoModelo,
                                                        @RequestParam(value = "anoFabricacaoDesde", required = false) Optional<String> anoFabricacaoDesde,
                                                        @RequestParam(value = "anoFabricacaoAte", required = false) Optional<String> anoFabricacaoAte,
                                                        @RequestParam(value = "sort", required = false) Optional<String> sort) {
        final var query = Query.builder().lote(lote).marca(marca).modelo(modelo)
                .anoFabricacao(anoFabricacao).anoModelo(anoModelo).anoFabricacaoDesde(anoFabricacaoDesde).anoFabricacaoAte(anoFabricacaoAte).sort(sort).build();
        final var veiculos = this.veiculoService.find(query);
        return ResponseEntity.ok(veiculos);
    }

    @GetMapping("/{id}")
    @Operation(tags = "veiculos", operationId = "getVeiculoPorId", description = "Operação para buscar um veículo pelo identificador", parameters = {
            @Parameter(name = "id", description = "Identificador único do veículo")
    }, responses = {
            @ApiResponse(responseCode = "200", description = "Operação realizada com sucesso"),
            @ApiResponse(responseCode = "404", description = "Veículo não encontrado",content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Void.class))
            })
    })
    public ResponseEntity<VeiculoData> veiculo(@PathVariable("id") String id) {
        VeiculoData veiculo = this.veiculoService.find(id);
        return ResponseEntity.ok(veiculo);
    }

    @DeleteMapping("/{id}")
    @Operation(tags = "veiculos", operationId = "delete", description = "Operação para excluir um veículo pelo identificador", parameters = {
            @Parameter(name = "id", description = "Identificador único do veículo")
    }, responses = {
            @ApiResponse(responseCode = "404", description = "Veículo não encontrado",content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Void.class))
            })
    })
    public ResponseEntity<Void> deleteVeiculo(@PathVariable("id") String id) {
        this.veiculoService.remove(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    @Operation(tags = "veiculos", operationId = "update", description = "Operação para atualizada completa de um veículo", parameters = {
            @Parameter(name = "id", description = "Identificador único do veículo")
    }, responses = {
            @ApiResponse(responseCode = "404", description = "Veículo não encontrado",content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Void.class))
            })
    })
    public ResponseEntity<VeiculoData> updateVeiculo(@PathVariable("id") String id, @RequestBody RequestAlteracaoVeiculo veiculo) {
        VeiculoData update = this.veiculoService.update(id, veiculo);
        return ResponseEntity.ok(update);
    }

    @PostMapping
    @Operation(tags = "veiculos", operationId = "criacao", description = "Operação para criação de um veículo")
    public ResponseEntity<VeiculoData> criarVeiculo(@Valid @RequestBody RequestNovoVeiculo veiculo, UriComponentsBuilder uriComponentsBuilder) {
        VeiculoData veiculoCriado = this.veiculoService.criar(veiculo);
        return ResponseEntity.created(uriComponentsBuilder.path("/api/veiculos/{id}").buildAndExpand(veiculoCriado.getId()).toUri()).body(veiculoCriado);
    }

}
