package tech.claudioed.auction.domain.resources;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;
import lombok.Builder;
import lombok.Value;
import org.apache.logging.log4j.util.Strings;
import tech.claudioed.auction.domain.data.response.legacy.compartilhado.RetornoVeiculo;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Value
@Builder
public class Query {

  private final Optional<String> lote;

  private final Optional<String> marca;

  private final Optional<String> modelo;

  private final Optional<String> anoFabricacao;

  private final Optional<String> anoModelo;

  private final Optional<String> anoFabricacaoDesde;

  private final Optional<String> anoFabricacaoAte;

  private final Optional<String> sort;

  public Predicate<RetornoVeiculo> create(){
    if (this.lote.isPresent())return retornoVeiculo -> retornoVeiculo.getLote().equalsIgnoreCase(this.lote.get());
    if (this.marca.isPresent())return retornoVeiculo -> retornoVeiculo.getMarca().equalsIgnoreCase(this.marca.get());
    if (this.modelo.isPresent())return retornoVeiculo -> Strings.isNotEmpty(retornoVeiculo.getModelo()) && retornoVeiculo.getModelo().toLowerCase().startsWith(this.modelo.get().toLowerCase());
    if (this.anoFabricacao.isPresent() && anoModelo.isPresent())return retornoVeiculo -> Strings.isNotEmpty(retornoVeiculo.getAnoFabricacao()) && Strings.isNotEmpty(retornoVeiculo.getAnoFabricacao()) && retornoVeiculo.getAnoFabricacao().equals(this.anoFabricacao.get()) && retornoVeiculo.getAnoModelo().equals(this.anoModelo.get());
    if (this.anoFabricacaoDesde.isPresent() && anoFabricacaoAte.isPresent())return retornoVeiculo -> retornoVeiculo.estaEntreAnoFabricacao(anoFabricacaoDesde.get(),anoFabricacaoAte.get());

    throw new IllegalArgumentException("query nao suportada");
  }

  public boolean isEmpty(){
    return this.lote.isEmpty() && this.marca.isEmpty() && this.modelo.isEmpty() && this.anoFabricacao.isEmpty() && this.anoModelo.isEmpty() && this.anoFabricacaoAte.isEmpty() && this.anoFabricacaoDesde.isEmpty();
  }

  public Comparator<RetornoVeiculo> order(){
    if(this.sort.isPresent()){
      if("asc".equalsIgnoreCase(this.sort.get())){
        return Comparator.comparing(RetornoVeiculo::dataLance);
      }else if("desc".equalsIgnoreCase(this.sort.get())){
        return (a, b) -> b.dataLance().compareTo(a.dataLance());
      }
    }
    return Comparator.comparing(RetornoVeiculo::getId);
  }

}
