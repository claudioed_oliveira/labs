package tech.claudioed.auction.domain.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EntityNotFound extends RuntimeException{

    private final String id;

    private final String entidade;

    public EntityNotFound(String id, String entidade){
        super(String.format("Entidade com id %s nao encontrada", id));
        this.id = id;
        this.entidade = entidade;
    }

}
