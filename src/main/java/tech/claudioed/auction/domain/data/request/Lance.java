package tech.claudioed.auction.domain.data.request;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(name = "InformacaoLance",title = "InformacaoLance",description = "Informações do último lance dado")
public class Lance {

  @Schema(name = "valorLance",title = "valor lance",description = "Valor do último lance dado")
  @NotNull(message = "Valor do último lance não pode ser vazio")
  @Positive(message = "Valor do último lance deve ser positivo")
  private BigDecimal valorLance;

  @Schema(name = "usuarioLance",title = "usuario lance",description = "Usuário cadastrado na plataforma que fez o último lance")
  @NotBlank(message = "Usuário do último lance não pode ser vazio")
  private String usuarioLance;

  @Schema(name = "dataLance",title = "data lance",description = "Data/hora que foi realizado o último lance")
  @NotBlank(message = "Data do último lance não pode ser vazio")
  private String dataLance;

}

