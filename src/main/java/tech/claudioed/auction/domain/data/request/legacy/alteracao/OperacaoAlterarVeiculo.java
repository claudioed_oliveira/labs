package tech.claudioed.auction.domain.data.request.legacy.alteracao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.claudioed.auction.domain.data.request.RequestAlteracaoVeiculo;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
public class OperacaoAlterarVeiculo {

  @JsonProperty("OPERACAO")
  private String operacao;

  @JsonProperty("VEICULO")
  private AlteracaoVeiculo veiculo;

  public static OperacaoAlterarVeiculo from(RequestAlteracaoVeiculo alteracaoVeiculo, String id){
    OperacaoAlterarVeiculo lance = new OperacaoAlterarVeiculo();
    lance.operacao = "alterar";
    lance.veiculo = AlteracaoVeiculo.builder()
        .anoFabricacao(alteracaoVeiculo.getVeiculo().getAnoFabricacao())
        .anoModelo(alteracaoVeiculo.getVeiculo().getAnoModelo())
        .codigoControle(alteracaoVeiculo.getVeiculo().getCodigoControle())
        .dataLance(alteracaoVeiculo.getLance().getDataLance())
        .lote(alteracaoVeiculo.getVeiculo().getLote())
        .marca(alteracaoVeiculo.getVeiculo().getMarca())
        .modelo(alteracaoVeiculo.getVeiculo().getModelo())
        .usuarioLance(alteracaoVeiculo.getLance().getUsuarioLance())
        .valorLance(alteracaoVeiculo.getLance().getValorLance().toString())
        .id(Integer.parseInt(id))
        .build();
    return lance;
  }

}
