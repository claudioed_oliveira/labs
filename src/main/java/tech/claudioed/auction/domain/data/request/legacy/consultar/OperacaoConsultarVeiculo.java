package tech.claudioed.auction.domain.data.request.legacy.consultar;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
public class OperacaoConsultarVeiculo {

  @JsonProperty("OPERACAO")
  private String operacao;

  public static OperacaoConsultarVeiculo from(){
    OperacaoConsultarVeiculo operacao = new OperacaoConsultarVeiculo();
    operacao.operacao = "consultar";
    return operacao;
  }

}
