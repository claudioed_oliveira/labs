package tech.claudioed.auction.domain.data.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "InformacaoVeiculo",title = "InformacaoVeiculo",description = "Informações do veículo")
public class Veiculo {

  @Schema(name = "marca",title = "marca",description = "Marca do veículo")
  @NotBlank(message = "Marca do veículo não pode ser vazio")
  private String marca;

  @Schema(name = "modelo",title = "modelo",description = "Modelo do veículo")
  @NotBlank(message = "Modelo do veículo não pode ser vazio")
  private String modelo;

  @Schema(name = "anoFabricacao",title = "ano fabricacao",description = "Ano de fabricação do veículo")
  @NotBlank(message = "Ano Fabricação do veículo não pode ser vazio")
  private String anoFabricacao;

  @Schema(name = "anoModelo",title = "ano modelo",description = "Ano do modelo do veículo")
  @NotBlank(message = "Ano modelo do veículo não pode ser vazio")
  private String anoModelo;

  @Schema(name = "lote",title = "lote",description = "Agrupador de um conjunto de veículos")
  @NotBlank(message = "Lote do veículo não pode ser vazio")
  private String lote;

  @Schema(name = "codigoControle",title = "codigo controle",description = "Código único do veículo dentro do lote")
  @NotBlank(message = "Código de controle do veículo não pode ser vazio")
  private String codigoControle;

}
