package tech.claudioed.auction.domain.data.response.legacy.exclusao;

import lombok.Data;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
public class RetornoExclusao {

  private String mensagem;

}
