package tech.claudioed.auction.domain.data.request.legacy.exclusao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
public class OperacaoExclusaoVeiculo {

  @JsonProperty("OPERACAO")
  private String operacao;

  @JsonProperty("VEICULO")
  private ExclusaoVeiculo veiculo;

  public static OperacaoExclusaoVeiculo from(String id){
    OperacaoExclusaoVeiculo exclusao = new OperacaoExclusaoVeiculo();
    exclusao.operacao = "apagar";
    exclusao.veiculo = ExclusaoVeiculo.builder()
        .id(Integer.parseInt(id)).build();
    return exclusao;
  }

}
