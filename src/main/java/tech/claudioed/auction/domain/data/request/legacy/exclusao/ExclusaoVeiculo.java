package tech.claudioed.auction.domain.data.request.legacy.exclusao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExclusaoVeiculo {

  @JsonProperty("ID")
  private Integer id;

}
