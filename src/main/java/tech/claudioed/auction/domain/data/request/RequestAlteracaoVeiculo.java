package tech.claudioed.auction.domain.data.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestAlteracaoVeiculo {

  @Schema(name = "lance",title = "lance",description = "Informações relacionadas ao último lance")
  private Lance lance;

  @Schema(name = "veiculo",title = "veiculo",description = "Informações relacionadas ao veículo")
  private Veiculo veiculo;

}
