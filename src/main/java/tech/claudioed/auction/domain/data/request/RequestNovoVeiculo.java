package tech.claudioed.auction.domain.data.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
@Valid
@Schema(name = "RequestNovoVeiculo",description = "Identifica informações necessárias para criaçāo de um novo veículo")
public class RequestNovoVeiculo {

  @Valid
  @NotNull
  @Schema(name = "lance",title = "lance",description = "Informações relacionadas ao último lance")
  private Lance lance;

  @Valid
  @NotNull
  @Schema(name = "veiculo",title = "veiculo",description = "Informações relacionadas ao veículo")
  private Veiculo veiculo;

}
