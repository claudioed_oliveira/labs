package tech.claudioed.auction.domain.data.request.legacy.criacao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.claudioed.auction.domain.data.request.RequestNovoVeiculo;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
public class OperacaoNovoVeiculo {

  @JsonProperty("OPERACAO")
  private String operacao;

  @JsonProperty("VEICULO")
  private CriacaoVeiculo veiculo;

  public static OperacaoNovoVeiculo from(RequestNovoVeiculo requestNovoVeiculo){
    OperacaoNovoVeiculo lance = new OperacaoNovoVeiculo();
    lance.operacao = "criar";
    lance.veiculo = CriacaoVeiculo.builder()
        .anoFabricacao(requestNovoVeiculo.getVeiculo().getAnoFabricacao())
        .anoModelo(requestNovoVeiculo.getVeiculo().getAnoModelo())
        .codigoControle(requestNovoVeiculo.getVeiculo().getCodigoControle())
        .dataLance(requestNovoVeiculo.getLance().getDataLance())
        .lote(requestNovoVeiculo.getVeiculo().getLote())
        .marca(requestNovoVeiculo.getVeiculo().getMarca())
        .modelo(requestNovoVeiculo.getVeiculo().getModelo())
        .usuarioLance(requestNovoVeiculo.getLance().getUsuarioLance())
        .valorLance(requestNovoVeiculo.getLance().getValorLance().toString())
        .build();
    return lance;
  }

}
