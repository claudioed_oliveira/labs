package tech.claudioed.auction.domain.data.request.legacy.alteracao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlteracaoVeiculo {

  @JsonProperty("ID")
  private Integer id;

  @JsonProperty("DATALANCE")
  private String dataLance;

  @JsonProperty("LOTE")
  private String lote;

  @JsonProperty("CODIGOCONTROLE")
  private String codigoControle;

  @JsonProperty("MARCA")
  private String marca;

  @JsonProperty("MODELO")
  private String modelo;

  @JsonProperty("ANOFABRICACAO")
  private String anoFabricacao;

  @JsonProperty("ANOMODELO")
  private String anoModelo;

  @JsonProperty("VALORLANCE")
  private String valorLance;

  @JsonProperty("USUARIOLANCE")
  private String usuarioLance;


}
