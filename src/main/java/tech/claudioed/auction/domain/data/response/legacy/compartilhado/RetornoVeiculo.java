package tech.claudioed.auction.domain.data.response.legacy.compartilhado;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import tech.claudioed.auction.domain.VeiculoData;
import tech.claudioed.auction.domain.data.request.Lance;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Data
public class RetornoVeiculo {

  private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm");

  @JsonProperty("ID")
  private String id;

  @JsonProperty("DATALANCE")
  private String dataLance;

  @JsonProperty("LOTE")
  private String lote;

  @JsonProperty("CODIGOCONTROLE")
  private String codigoControle;

  @JsonProperty("MARCA")
  private String marca;

  @JsonProperty("MODELO")
  private String modelo;

  @JsonProperty("ANOFABRICACAO")
  private String anoFabricacao;

  @JsonProperty("ANOMODELO")
  private String anoModelo;

  @JsonProperty("VALORLANCE")
  private String valorLance;

  @JsonProperty("USUARIOLANCE")
  private String usuarioLance;

  public VeiculoData convert(){
    Lance lance = Lance.builder().dataLance(this.dataLance).usuarioLance(this.usuarioLance).valorLance(BigDecimal.valueOf(Double.valueOf(this.valorLance))).build();
    return VeiculoData.builder().anoFabricacao(this.anoFabricacao).anoModelo(this.anoModelo).codigoControle(this.codigoControle)
            .modelo(this.modelo).marca(this.marca).lote(this.lote).lance(lance).id(this.id).build();
  }

  public LocalDateTime dataLance(){
    return LocalDateTime.parse(this.dataLance,formatter);
  }

  public boolean estaEntreAnoFabricacao(String desde,String ate){
    return anoFab() >= Integer.parseInt(desde) && anoFab() <= Integer.parseInt(ate);
  }

  private Integer anoFab(){
    return Integer.parseInt(Optional.ofNullable(this.anoFabricacao).orElse("0"));
  }

}
