package tech.claudioed.auction.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tech.claudioed.auction.domain.data.request.Lance;
import tech.claudioed.auction.domain.data.request.Veiculo;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "veiculo", description = "Dados do veículo")
public class VeiculoData {

  @Schema(name = "id", title = "id", description = "Identificador do veículo")
  private String id;

  @Schema(name = "marca", title = "marca", description = "Marca do veículo")
  private String marca;

  @Schema(name = "modelo",title = "modelo",description = "Modelo do veículo")
  private String modelo;

  @Schema(name = "anoFabricacao",title = "ano fabricacao",description = "Ano de fabricação do veículo")
  private String anoFabricacao;

  @Schema(name = "anoModelo",title = "ano modelo",description = "Ano do modelo do veículo")
  private String anoModelo;

  @Schema(name = "lote",title = "lote",description = "Agrupador de um conjunto de veículos")
  private String lote;

  @Schema(name = "codigoControle",title = "codigo controle",description = "Código único do veículo dentro do lote")
  private String codigoControle;

  @Schema(name = "lance",title = "lance",description = "Informações relacionadas ao último lance")
  private Lance lance;

}
