package tech.claudioed.auction.domain.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.claudioed.auction.domain.VeiculoData;
import tech.claudioed.auction.domain.data.request.RequestAlteracaoVeiculo;
import tech.claudioed.auction.domain.data.request.RequestNovoVeiculo;
import tech.claudioed.auction.domain.data.request.legacy.alteracao.OperacaoAlterarVeiculo;
import tech.claudioed.auction.domain.data.request.legacy.consultar.OperacaoConsultarVeiculo;
import tech.claudioed.auction.domain.data.request.legacy.criacao.OperacaoNovoVeiculo;
import tech.claudioed.auction.domain.data.request.legacy.exclusao.OperacaoExclusaoVeiculo;
import tech.claudioed.auction.domain.data.response.legacy.compartilhado.RetornoVeiculo;
import tech.claudioed.auction.domain.data.response.legacy.exclusao.RetornoExclusao;
import tech.claudioed.auction.domain.exception.EntityNotFound;
import tech.claudioed.auction.domain.resources.Query;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author claudioed on 06/10/20.
 * Project auction
 */
@Service
public class VeiculoService {

    private final String endpoint;

    private final RestTemplate restTemplate;

    public VeiculoService(@Value("${legacy.host}") String endpoint, RestTemplate restTemplate) {
        this.endpoint = endpoint;
        this.restTemplate = restTemplate;
    }

    public VeiculoData criar(RequestNovoVeiculo requestNovoVeiculo) {
        final var criacao = OperacaoNovoVeiculo.from(requestNovoVeiculo);
        final var responseEntity = this.restTemplate
                .postForEntity(this.endpoint, criacao, RetornoVeiculo.class);
        final var novoLanceRetorno = responseEntity.getBody();
        return novoLanceRetorno.convert();
    }

    public RetornoExclusao remove(String id) {
        VeiculoData veiculoCriado = find(id);
        final var exclusao = OperacaoExclusaoVeiculo.from(veiculoCriado.getId());
        final var responseEntity = this.restTemplate
                .postForEntity(this.endpoint, exclusao, RetornoExclusao.class);
        return responseEntity.getBody();
    }

    public VeiculoData find(String id) {
        final var entity = new HttpEntity<OperacaoConsultarVeiculo>(OperacaoConsultarVeiculo.from());
        final var response = this.restTemplate.exchange(this.endpoint, HttpMethod.POST, entity,
                new ParameterizedTypeReference<List<RetornoVeiculo>>() {
                });
        List<RetornoVeiculo> veiculos = Objects.requireNonNull(response.getBody()).stream()
                .filter(el -> el.getId().equals(id)).collect(Collectors.toList());

        if(veiculos.isEmpty()){
            throw new EntityNotFound(id, "veiculo");
        }
        return veiculos.get(0).convert();
    }

    public List<VeiculoData> find(Query query) {
        final var entity = new HttpEntity<OperacaoConsultarVeiculo>(OperacaoConsultarVeiculo.from());
        final var response = this.restTemplate.exchange(this.endpoint, HttpMethod.POST, entity,
                new ParameterizedTypeReference<List<RetornoVeiculo>>() {
                });
        if(query.isEmpty()){
            return Objects.requireNonNull(response.getBody()).stream().sorted(query.order()).map(RetornoVeiculo::convert).collect(Collectors.toList());
        }
        return Objects.requireNonNull(response.getBody()).stream()
                .filter(query.create()).sorted(query.order()).map(RetornoVeiculo::convert).collect(Collectors.toList());
    }

    public VeiculoData update(String id, RequestAlteracaoVeiculo requestAlteracaoVeiculo) {
        VeiculoData veiculoCriado = find(id);
        final var alteracao = OperacaoAlterarVeiculo.from(requestAlteracaoVeiculo,veiculoCriado.getId());
        final var responseEntity = this.restTemplate
                .postForEntity(this.endpoint, alteracao, RetornoVeiculo.class);
        return responseEntity.getBody().convert();
    }

}
